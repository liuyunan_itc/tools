<?php
/**
  * Generates an UUID
  *
  * @author     NaN
  * @param      string  an optional prefix
  * @return     string  the formatted uuid
  */
class Uuid{

	var $prefix = "";

	function uuid($prefix = '')
	{
		$this->prefix = $prefix;
	} 

	function getUuid()
	{
		$chars = md5(uniqid(mt_rand(), true));
		$uuid  = substr($chars,0,8) . '-';
		$uuid .= substr($chars,8,4) . '-';
		$uuid .= substr($chars,12,4) . '-';
		$uuid .= substr($chars,16,4) . '-';
		$uuid .= substr($chars,20,12);
		return $this->prefix . $uuid;
	}

	function setPrefix($prefix="")
	{
		$this->prefix = $prefix;
	}
}

 
//Example of using the function -
$uuid = new Uuid();
//Using without prefix.
echo $uuid->getUuid(); //Returns like ‘1225c695-cfb8-4ebb-aaaa-80da344e8352′  
 
//Using with prefix
$uuid->setPrefix("NaN:");
echo "<hr>";
echo $uuid->getUuid();//Returns like ‘urn:uuid:1225c695-cfb8-4ebb-aaaa-80da344e8352′
?>