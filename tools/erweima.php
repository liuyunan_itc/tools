<?php
require_once "QRcode.class.php";
//$QR = QRcode::png ('二维码测试',false,$level=QR_ECLEVEL_H);

// QR Code + Logo Generator QR图片中间加logo
$data = '兰博基尼';
$size = '200x200';
$logo = './logo.jpg';  //中间的logo图
$temp = "./temp.png";
// 通过google api生成未加logo前的QR图。
// $png = 'http://chart.googleapis.com/chart?chs=' . $size . '&cht=qr&chl=' . urlencode($data) . '&chld=H|1&choe=UTF-8';
// $QR = imagecreatefrompng($png);

QRcode::png ($data,$temp,$level=QR_ECLEVEL_Q,5,3);
$QR = imagecreatefrompng($temp);
if ($logo !== FALSE) {
  $logo = imagecreatefromstring(file_get_contents($logo));
  $QR_width = imagesx($QR);
  $QR_height = imagesy($QR);
  $logo_width = imagesx($logo);
  $logo_height = imagesy($logo);
  $logo_qr_width = $QR_width/5;
  $scale = $logo_width/$logo_qr_width;
  $logo_qr_height = $logo_height/$scale;
  $from_width = ($QR_width-$logo_qr_width)/2;
  imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width+10, $logo_qr_height+10, $logo_width, $logo_height);
}
header('Content-type: image/png');
imagepng($QR);
imagedestroy($QR);
?>