<?php

/**
 * @author gsj
 * @copyright 2012
 */

class Sqlite_DB{

    public $db_file = '';           //要打开的数据库文件
	public $version = ''; 			//sqlite版本
	private $link = null;			//获得的数据库连接
    public $error_msg = '';         //查询失败的错误文本信息
    
    // 构造函数
    public function __construct($db_file){
        $this->db_file = $db_file;
        $this->connet($db_file);
    }
   	/*++
	函数名：connet
	作者：gsj
	时间：2012.12.03
	描述：连接数据库
	参数：$db_file 数据库文件名
	返回值：连接数据库,成功返回连接资源，否则输出错误信息，结束脚本
	--*/   
    public function connet($db_file){
        
  		if(!extension_loaded("sqlite")){
			$this->halt("请打开sqlite相关扩展");
		}
        if($this->link){
            return $this->link;
        }else{
            if(!$this->link = sqlite_open($db_file, 0666, $sqliteerror)){
                $this->halt($sqliteerror);
            }else{
                return $this->link;
            }
        }
    }
   	/*++
	函数名：getTables
	作者：gsj
	时间：2012.12.03
	描述：获取所有存在的表
	参数：无
	返回值：返回所有表组成的数组
	--*/
    public function getTables(){
        $tables = array();
        $sql = "select name from sqlite_master where type='table'";
        $result = $this->fetchAll($sql);
        foreach($result as $value){
            $tables[] = $value["name"];
        }
        return $tables;
    }
 	/*++
	函数名：dropTables
	作者：gsj
	时间：2012.12.04
	描述：删除数据库中所有表
	参数：无
	返回值：成功返回true，如果查询执行不正确则返回 false。
	--*/
	public function dropTables(){
  
		$tables = $this->getTables();   
		foreach($tables as $table) {
            if(!$this->query("drop table $table")){
                return false;
            }   
		} 
		return true;
	}
   	/*++
	函数名：checkTable
	作者：gsj
	时间：2012.12.03
	描述：检查表是否存在
	参数：$table_name 表名
	返回值：存在返回true，否则返回false
	--*/
	public function checkTable($table_name) {
		
        $tables = $this->getTables();
        return in_array($table_name,$tables);		
	}
	/*++
	函数名：query
	作者：gsj
	时间：2012.12.04
	描述：执行查询语句
	参数：$sql   查询语句
	返回值：返回一个资源标识符，如果查询执行不正确则返回 FALSE。
	--*/
	public function query($sql,$type = '') {		
		return @sqlite_query($this->link,$sql,SQLITE_BOTH,$this->error_msg);		 
	}
   	/*++
	函数名：fetchArray
	作者：gsj
	时间：2012.12.04
	描述：执行查询语句
	参数：$query 查询结构资源标示符，$type返回的数组类型，默认为关联数组
	返回值：返回一行的fetch结果
	--*/ 
    public function fetchArray($query,$type=SQLITE_ASSOC){
        return sqlite_fetch_array($query,$type); 
    }
	/*++
	函数名：fetchAll
	作者：gsj
	时间：2012.12.04
	描述：获取结果集
	参数： $sql 查询语句,$type返回的数组类型，默认为关联数组
	返回值：返回查询语句执行后的所有结果的二维数组
	--*/
    public function fetchAll($sql,$type=SQLITE_ASSOC){
        return sqlite_fetch_all($this->query($sql),$type);
    }
   	/*++
	函数名：resultFirst
	作者：gsj
	时间：2012.12.04
	描述：取得第一行第一列的值
	参数：$sql   查询语句
	返回值：取得第一列第一行的值
	--*/
	public function resultFirst($sql) {
	   $result = $this->fetchArray($this->query($sql),$type=SQLITE_NUM);
	   return $result[0];
	}
 	/*++
	函数名：numRows
	作者：gsj
	时间：2012.12.04
	描述：获取查询结果的行数
	参数：$sql 查询语句执行后的资源描述符
	返回值：获取查询结果的行数
	--*/
	public function numRows($query) {
		$result_rows = sqlite_num_rows($query);
		return $result_rows;
	}
	
	/*++
	函数名：countRows
	作者：gsj
	时间：2012.12.04
	描述：获取查询结果的行数
	参数：$sql 查询语句
	返回值：获取查询结果的行数
	--*/
	public function countRows($sql) {
		$query = $this->query($sql);
		$result_rows = sqlite_num_rows($query);
		return $result_rows;
	}
 	/*++
	函数名：insertId
	作者：gsj
	时间：2012.12.04
	描述：获得某个表的最大ID号,自增列:integer primary key
	参数：$table 表名，$id  主键
	返回值：返回表的最大id值， 如果为自增列，则可以认为是最后次插入的记录的id
	--*/
	public function insertId($table,$id) {
	   $sql = "select max($id) from $table";
	   return $this->resultFirst($sql);
	}
 	/*++
	函数名：numFields
	作者：gsj
	时间：2012.12.04
	描述：取得结果集中字段的数目
	参数：$query 查询语句执行后返回的资源描述符
	返回值：取得结果集中字段的数目
	--*/
	public function numFields($query) {
		return sqlite_num_fields($query);
	}
 	/*++
	函数名：getError
	作者：gsj
	时间：2012.12.04
	描述：返回上一个 sqlite 查询产生的文本错误信息
	参数：无
	返回值：返回上一个 sqlite 查询产生的文本错误信息
	--*/
	public function getError() {
        return $this->error_msg;
	}
 	/*++
	函数名：getErrno
	作者：gsj
	时间：2012.12.04
	描述：返回上一个 sqlite 操作中的错误信息的数字编码
	参数：无
	返回值：返回上一个 sqlite 操作中的错误信息的数字编码
	--*/
	public function getErrno() {
		return sqlite_last_error($this->link);
	}
 	/*++
	函数名：getVersion
	作者：gsj
	时间：2012.12.04
	描述：取得 sqlite 版本信息 ,此处根据创建的db文件信息读取，若不同文件格式不同，可能有问题
	参数：无
	返回值：返回sqlite 版本信息
	--*/
	public function getVersion() {        
        if(!$lines = file($this->db_file))
            $this->halt("获取版本信息失败");
        $sqliteinfo = explode("**",$lines[0]);
        $versioninfo = explode(" ",$sqliteinfo[1]);
        $this->version = $versioninfo[5]." ".$versioninfo[6];
        return $this->version;
	}
 	/*++
	函数名：halt
	作者：gsj
	时间：2012.12.04
	描述：结束脚本，并输出相关信息
	参数： $msg 输出信息字符串
	返回值：无
	--*/
    public function halt($msg){
        die($msg);
    }
 	/*++
	函数名：close
	作者：gsj
	时间：2012.12.04
	描述：释放获取的数据库连接资源
	参数： 无
	返回值：无
	--*/
    public function close(){
        if($this->link){
            sqlite_close($this->link);
        }     
    }
   	//析构函数
	public function __destruct(){
		$this->close();
	}
}

?>