<?php
//大文件操作，SplFileObject类
//php version >= 5.1.2
//SplFileObject继承自SplFileInfo，因此，也可用它读取文件属性等
try{
	$file = new SplFileObject("bing.txt");

	echo date("Y-m-d H:i:s",$file->getATime()+8*3600);//上次访问时间
	echo "<hr>";
	echo date("Y-m-d H:i:s",$file->getCTime()+8*3600);//创建时间
	echo "<hr>";
	echo $file->getFileName(); //文件名称
	echo "<hr>";
	echo $file->getSize(); //文件大小
	echo "<hr>";
	while ( ! $file->eof()) {

	    echo $file->fgets();

		echo "</br>";

	}
}catch(Exception $e){ //文件不存在时，抛出RuntimeException异常
	echo $e->getMessage();
}

?>