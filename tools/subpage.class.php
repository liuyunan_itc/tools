<?php
/**
* $sql语句：①获取数据②获取总记录数
*/
class subPage{
	public $pageSize=5;//每页显示的数量-->程序员指定的
	public $rowCount=52;//这是从数据库中获取的（形如SELECT COUNT(id) FROM TABLE）用来保存总共有多少条记录
	public $pageNow;//通过$_GET['page']获取的,用来保存当前所在的页码
	public $pageCount;//计算得到的,用来保存总共有多少页
	public $pageShow=5;//限制每个页面显示的最多分页数
	public $res_arr;//用来保存要显示到页面的数据(比如保存SELECT * FROM TABLE LIMIT 0,10 检索的数据)
	public $nav;//显示第几页第几页的导航条
	public $redUrl="subPage.class.php";//分页跳转的地址
	
	function subPage()
	{
		$this->pageNow = isset($_GET["page"])?$_GET["page"]:1;
		echo $this->pageNow."<hr>";
	}
	/**
	* 取得当前页面的超链接
	*/
	public function getLink()
	{
		$this->nav='';
		$this->pageCount=ceil(($this->rowCount/$this->pageSize));
		$step= floor(($this->pageNow-1)/$this->pageShow)*$this->pageShow+1;
		if ($this->pageNow>$this->pageShow)
		{
			$this->nav.=" <a href='".$this->redUrl."?page=".($step-1)."'><<</a> &nbsp;";//整体每$this->pageShow页向前翻
		}
		if ($this->pageNow!=1)
		{
			$this->nav.="<a href='".$this->redUrl."?page=1'>首页</a> ";
			$this->nav.="<a href='".$this->redUrl."?page=".($this->pageNow-1)."'>上一页</a> ";
		}
		for ($start=$step;$start<$step+$this->pageShow && $start<=$this->pageCount;$start++)
		{
			if($this->pageNow == $start)
			{
				$this->nav.="<a>".$start."</a> ";
				continue;
			}
			$this->nav.="<a href='".$this->redUrl."?page=".$start."'>".$start."</a> ";
		}
		if ($this->pageNow!=$this->pageCount)
		{
			$this->nav.=" <a href='".$this->redUrl."?page=".($this->pageNow+1)."'>下一页</a>&nbsp;";
			$this->nav.="<a href='".$this->redUrl."?page=".$this->pageCount."'>末页</a> ";
		}
		if ($this->pageCount>$this->pageShow && $this->pageNow<$this->pageCount-$this->pageShow)
		{
			$this->nav.=" <a href='".$this->redUrl."?page=".($step+$this->pageShow)."'>>></a>";//整体每$this->pageShow页向后翻
		}
		$this->nav.="&nbsp;---共有".$this->pageCount."页";
	}
}
//测试代码
$fenye = new subPage();
$fenye->getLink();
echo $fenye->nav;
?> 