﻿<?php
/*++
功能：数据库操作类
作者：gsj
时间：2012.5.1
描述：数据库操作类，初始化连接，关闭连接，选择数据库，执行查询语句等等
文件名称：db.class.php
--*/

//数据库操作类，对原来php提供的函数进行二次封装
class DB {
	var $dbhost = "localhost";  //主机名
	var $dbuser = "root";		//登录名
	var $dbpw = "";	//登录密码
	var $pconnect = 0;			//连接方式
	var $halt = 1;
	var $version = ''; 			//mysql版本
	//var $querynum = 0; 
	var $link = null;			//获得的数据库连接

	/*++
	函数名：result_first
	作者：gsj
	时间：2012.05.01
	描述：取得第一行第一列的值
	参数：$sql   查询语句
	返回值：取得第一列第一行的值
	--*/
	function result_first($sql) {
		return $this->result($this->query($sql), 0);
	}
	
	/*++
	函数名：query
	作者：gsj
	时间：2012.05.01
	描述：执行查询语句
	参数：$sql   查询语句
	返回值：返回一个资源标识符，如果查询执行不正确则返回 FALSE。
	--*/
	function query($sql, $type = '') {
		
		$query = mysql_query($sql, $this->link);
		return $query;
	}
	
	/*++
	函数名：affected_rows
	作者：gsj
	时间：2012.05.01
	描述：获得上次查询影响的行数
	参数：无
	返回值：则返回受影响的行的数目，如果最近一次查询失败的话，函数返回 -1
	--*/
	function affected_rows() {
		return mysql_affected_rows($this->link);
	}
	
	/*++
	函数名：errno
	作者：gsj
	时间：2012.05.01
	描述：返回上一个 MySQL 操作中的错误信息的数字编码
	参数：无
	返回值：返回上一个 MySQL 操作中的错误信息的数字编码
	--*/
	function errno() {
		return intval(($this->link) ? mysql_errno($this->link) : mysql_errno());
	}
	
	/*++
	函数名：free_result
	作者：gsj
	时间：2012.05.01
	描述：释放结果内存
	参数：$query 查询语句执行后返回的数据指针
	返回值：释放结果内存
	--*/
	function free_result($query) {
		return mysql_free_result($query);
	}
	
	/*++
	函数名：insert_id
	作者：gsj
	时间：2012.05.01
	描述：获得上一步操作产生的ID号
	参数：无
	返回值：返回上一步 INSERT 操作产生的 ID。如果上一查询没有产生 AUTO_INCREMENT 的 ID，则 mysql_insert_id() 返回 0
	--*/
	function insert_id() {
		return ($id = mysql_insert_id($this->link)) >= 0 ? $id : $this->result($this->query("SELECT last_insert_id()"), 0);
	}

	/*++
	函数名：fetch_row
	作者：gsj
	时间：2012.05.01
	描述：从结果集中取得一行作为数字数组
	参数：$query 查询语句执行后返回的数据指针
	返回值：返回处理的数字数组
	--*/
	function fetch_row($query) {
		$query = mysql_fetch_row($query);
		return $query;
	}
	
	/*++
	函数名：fetch_fields
	作者：gsj
	时间：2012.05.01
	描述：从结果集中取得列信息并作为对象返回
	参数：$query 查询语句执行后返回的数据指针
	返回值：返回处理的数字数组
	--*/
	function fetch_fields($query) {
		return mysql_fetch_field($query);
	}
	
	/*++
	函数名：version
	作者：gsj
	时间：2012.05.01
	描述：取得 MySQL 服务器信息
	参数：无
	返回值：返回服务器的相关信息
	--*/
	function version() {
		if(empty($this->version)) {
			$this->version = mysql_get_server_info($this->link);
		}
		return $this->version;
	}
	
	/*++
	函数名：close
	作者：gsj
	时间：2012.05.01
	描述：关闭取得的数据库连接
	参数：无
	返回值：无
	--*/
	function close() {
		return mysql_close($this->link);
	}
	
	//停止
	function halt($message = '', $sql = '') {
		define('CACHE_FORBIDDEN', TRUE);
		echo $this->error();
		require_once 'db_error.inc.php';
	}
	
	/*++
	函数名：check_table_exists
	作者：gsj
	时间：2012.05.01
	描述：检查表是否存在
	参数：$table_name 表名
	返回值：存在返回true，否则返回false
	--*/
	function check_table_exists($table_name) {
		
			$result = $this->query("SHOW TABLES");
			$tables_in ='Tables_in_db_deamon';
			while ($row = $this->fetch_array($result)) {
				if ($row[$tables_in] == $table_name)
					return true;
			}
			return false;
		
	}
    
   	//析构函数
	public function __destruct(){
		$this->close();
	} 
}

?>
